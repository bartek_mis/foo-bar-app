'use strict';

angular
    .module('app', [
        'ngMaterial',
        'ui.router',
        'Home',
        'News',
        'Gallery'
    ])
    .config(appConfig)
    .run(appRun);

appConfig.$inject = ['$urlRouterProvider'];
function appConfig($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
}

appRun.$inject = ['$rootScope', '$state', '$stateParams'];
function appRun($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}

// Home
angular
    .module('Home', [])
    .config(HomeConfig);

HomeConfig.$inject = ['$stateProvider'];
function HomeConfig($stateProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: './templates/home.html'
        });
}

// NASA News
angular
    .module('News', [])
    .config(NewsConfig)
    .controller('NewsController', NewsController);

NewsConfig.$inject = ['$stateProvider'];
function NewsConfig($stateProvider) {
    $stateProvider
        .state('news', {
            url: '/news',
            templateUrl: './templates/news.html',
            controller: 'NewsController as news',
            resolve: {
                News: ['$http', function($http) {
                    return $http.get('https://api.nasa.gov/planetary/apod?api_key=BHHHQyyoaRjoFSQjRtoD0xt1vCdc0MgioFRrqxwW')
                                    .then(function(response) {
                                        return response.data;
                                    });
                }]
            }
        });
}

NewsController.$inject = ['News'];
function NewsController(News) {
    var news = this;
    news.data = News;
}

// Gallery
angular
    .module('Gallery', [])
    .config(GalleryConfig);

GalleryConfig.$inject = ['$stateProvider'];
function GalleryConfig($stateProvider) {
    $stateProvider
        .state('gallery', {
            url: '/gallery',
            templateUrl: './templates/gallery.html'
        });
}